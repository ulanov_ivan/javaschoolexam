package com.tsystems.javaschool.tasks.pyramid;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        // TODO : Implement your solution here

        boolean flag;
        int[][] matrix;
        int size = inputNumbers.size();
        int maxValue = Integer.MAX_VALUE - 1;

        if (inputNumbers.contains(null)) {
            throw new CannotBuildPyramidException();
        }
        if (size == maxValue) {
            throw new CannotBuildPyramidException();
        }
        int count = 0;
        int rows = 1;
        int columns = 1;

        while (count < size) {
            count += rows;
            rows++;
            columns += 2;
        }
        rows -= 1;
        columns -= 2;
        flag = size == count;

        if (flag) {
            List<Integer> sorted = inputNumbers.stream().sorted().collect(Collectors.toList());
            matrix = new int[rows][columns];
            for (int[] row : matrix) {
                Arrays.fill(row, 0);
            }
            int center = (columns / 2);
            count = 1;
            int arrIdx = 0;

            for (int i = 0, offset = 0; i < rows; i++, offset++, count++) {
                int start = center - offset;
                for (int j = 0; j < count * 2; j += 2, arrIdx++) {
                    matrix[i][start + j] = sorted.get(arrIdx);
                }
            }
            for (int[] a : matrix) {
                for (int b : a)
                    System.out.print(b + " ");
                System.out.println();
            }
        } else {
            throw new CannotBuildPyramidException();
        }
        return matrix;
    }


}
