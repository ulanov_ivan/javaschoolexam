package com.tsystems.javaschool.tasks.calculator;

import java.math.BigDecimal;

interface Operand {
    BigDecimal eval();
}